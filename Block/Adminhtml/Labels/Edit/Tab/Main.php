<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Relabel
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 */
namespace Shero\Relabel\Block\Adminhtml\Labels\Edit\Tab;

class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $store;

    /**
     * @var \Magento\Backend\Block\Template\Context
     */
    protected $contex;

    /**
     * @var \Magento\Config\Model\Config\Source\Store
     */
    protected $storeSource;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Config\Model\Config\Source\Store $storeSource,
        array $data = []
    ) {
        $this->context = $context;
        $this->storeSource = $storeSource;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {


        $groups = $this->_coreRegistry->registry('translation_data');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();


        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Translation Information')]);

        if ($groups != "") {
            $fieldset->addField('key_id', 'hidden', ['name' => 'key_id']);
        }


        $fieldset->addField(
            'string',
            'text',
            [
                'name' => 'string',
                'label' => __('Original Label'),
                'title' => __('Original Label'),
                'required' => true,
            ]
        );

        $fieldset->addField(
            'translate',
            'text',
            [
                'name' => 'translate',
                'label' => __('Translated Label'),
                'title' => __('Translated Label'),
                'required' => true,
            ]
        );
        $fieldset->addField(
            'store_id',
            'select',
            [
                'name' => 'store_id',
                'label' => __('Store'),
                'title' => __('Store'),
                'required' => true,
                'values' =>$this->getStores(),
            ]
        );

        if ($groups != "") {
            $form->setValues($groups->getData());
        }

        $this->getStores();

        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Translation Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Translation Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    /**
     * @return array
     */
    public function getStores(){

        return $this->storeSource->toOptionArray();

    }
}

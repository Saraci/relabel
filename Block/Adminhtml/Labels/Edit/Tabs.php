<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Relabel
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 */
namespace Shero\Relabel\Block\Adminhtml\Labels\Edit;
/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    protected function _construct()
    {
        parent::_construct();
        $this->setId('labels_tab');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Translation Information'));
    }
}

<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Relabel
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 */
namespace Shero\Relabel\Block\Adminhtml\Labels;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    /**
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'key_id';
        $this->_blockGroup = 'Shero_Relabel';
        $this->_controller = 'adminhtml_labels';

        parent::_construct();

        $this->buttonList->update('save', 'label', __('Save Label'));
        $this->buttonList->add(
            'saveandcontinue',
            [
                'label' => __('Save and Continue Edit'),
                'class' => 'save',
                'data_attribute' => [
                    'mage-init' => [
                        'button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form'],
                    ],
                ]
            ],
            -100
        );

        $this->buttonList->update('delete', 'label', __('Delete Label'));
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    /**
     * Getter of url for "Save and Continue" button
     * tab_id will be replaced by desired by JS later
     *
     * @return string
     */
    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('labels/*/save', ['_current' => true, 'back' => 'edit', 'active_tab' => '']);
    }

    public function getValidationUrl()
    {
        return $this->getUrl('relabel/*/validate', ['_current' => true]);
    }
    public function getBackUrl()
    {
        return $this->getUrl('*/grid');
    }

}

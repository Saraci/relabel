<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Relabel
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 */
namespace Shero\Relabel\Api\Data;

interface RelabelInterface extends \Magento\Framework\Api\CustomAttributesDataInterface
{
    const LABEL_ID      = 'key_id';
    const LABEL_STRING    = 'string';
    const LABEL_STORE_ID   = 'store_id';
    const LABEL_TRANS = 'translate';
    const LABEL_LOCALE = 'locale';
    const LABEL_CRC_STRING = 'crc_string';


    public function getLabelId();

    public function setLabelId($entityId);

    public function getLabelString();

    public function setLabelString($string);

    public function getLabelStoreId();

    public function setLabelStoreId($storeId);

    public function getLabelTranslation();

    public function setLabelTranslation($translation);

    public function getLabelLocale();

    public function setLabelLocale($locale);

    public function getLabelCrc();

    public function setLabelCrc($crc_string);
}
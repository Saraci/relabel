<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Relabel
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 */
namespace Shero\Relabel\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();
        
        $table = $installer->getConnection()->newTable(
            $installer->getTable('shero_relabel_translation')
        )->addColumn(
            'key_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Key Id'
        )->addColumn(
            'string',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'String to be translated'
        )->addColumn(
            'store_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            20,
            ['nullable' => false,'default' => 0],
            'Store id'
        )->addColumn(
           'translate',
           \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
           ['nullable' => true],
           'Translate string'
        )->addColumn(
            'locale',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            20,
            ['nullable' => false, 'default' => 'en_US'],
            'Locale'
        )->addColumn(
            'crc_string',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            20,
            ['nullable' => false, 'default' => 1591228201],
            'Translation String CRC32 Hash'
        )
            ->setComment(
            'Translations Table'
        );

        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    }
}
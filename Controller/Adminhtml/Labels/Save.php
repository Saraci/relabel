<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Relabel
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 */
namespace Shero\Relabel\Controller\Adminhtml\Labels;

use Magento\Framework\App\Cache\TypeListInterface as CacheTypeListInterface;
use Shero\Relabel\Model\RelabelFactory;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\App\Cache\CacheTypeListInterface
     */
    protected $cache;

    /**
     * @var \Shero\Relabel\Model\RelabelFactory
     */
    protected $_relabelFactory;

    /**
     * Save constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Cache\CacheTypeListInterface $cache
     * @param \Magento\Framework\App\Cache\CacheManager $cacheManager
     * @param \Shero\Relabel\Model\RelabelFactory $relabelFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        CacheTypeListInterface $cache,
        RelabelFactory $relabelFactory
    ) {

        parent::__construct($context);
        $this->cache = $cache;
        $this->_relabelFactory = $relabelFactory;

    }

    public function execute(){

        $params = $this->getRequest()->getParams();

            $go_back = isset($params['back'])?true:false;

            // init model and delete
            $params = $this->getRequest()->getParams();

            $originalLabel = $params['string'];
            $translatedLabel = $params['translate'];
            $storeId = $params['store_id'];

            $model = $this->_relabelFactory->create();

        /**
         * update the row if original label is the same
         */
            $key_id = $model->getKeyIdByString($originalLabel,$storeId);

            if($key_id){
                $params['key_id'] = $key_id;
            }

            if(isset($params['key_id'])) {
                try {
                    $id = $params['key_id'];
                    $item = $model->load($id);
                    $item->setLabelString($originalLabel);
                    $item->setLabelTranslation($translatedLabel);
                    $item->setLabelCrc($translatedLabel);
                    $item->setStoreId($storeId);
                    $item->save();
                    $this->cache->invalidate(['full_page','translate']);
                }
                catch(\Exception $e){
                    $this->messageManager->addError($e->getMessage());
                    $url = $this->getUrl("*/*/edit");
                    $this->_redirect($url);
                }
            }
            else {
                try {
                    $model->setLabelString($originalLabel);
                    $model->setLabelTranslation($translatedLabel);
                    $model->setStoreId($storeId);
                    $model->setLabelCrc($translatedLabel);
                    $model->save();
                    $this->cache->invalidate(['full_page','translate']);
                    $id = $model->getLabelId();
                }catch(\Exception $e){

                    $this->messageManager->addError($e->getMessage());
                    $url = $this->getUrl("*/*/*");
                    $this->_redirect($url);
                }
            }

            $this->messageManager->addSuccess(__('The Label Translation is saved!'));
            if($go_back){
                $url = $this->getUrl("*/*/edit",array("key_id"=>$id));
                $this->_redirect($url);
            }else{
                $this->_redirect("*/grid/");
            }
        }
}

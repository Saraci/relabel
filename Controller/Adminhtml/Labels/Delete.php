<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Relabel
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 */
namespace Shero\Relabel\Controller\Adminhtml\Labels;

use Shero\Relabel\Model\RelabelFactory;

class Delete extends \Magento\Backend\App\Action
{

    /**
     * @var Shero\Relabel\Model\RelabelFactory
     */
    protected $_relabelFactory;


    /**
     * Delete constructor.
     * @param \Shero\Relabel\Model\RelabelFactory $relabelFactory
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct
    (
        RelabelFactory $relabelFactory,
        \Magento\Backend\App\Action\Context $context
    )
    {
        $this->_relabelFactory = $relabelFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('key_id');

        if (!$id) {
            $id = $this->getRequest()->getParam('id');
        }
        try {
             // init model and delete
            $model = $this->_relabelFactory->create();
            $model->load($id);
            $model->delete();

             // display success message
            $this->messageManager->addSuccess(__('You deleted the label translation.'));
            // go to grid
            $url = $this->getUrl("*/grid");

            $this->_redirect($url);

        } catch (\Exception $e) {
            // display error message
            $this->messageManager->addError($e->getMessage());
            // go back to edit form
            $url = $this->getUrl("*/*/edit", array("key_id" => $id));
            $this->_redirect($url);
        }

    }
}

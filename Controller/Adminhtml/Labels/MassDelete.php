<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Relabel
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 */
namespace Shero\Relabel\Controller\Adminhtml\Labels;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Shero\Relabel\Model\ResourceModel\Relabel\CollectionFactory;
use Magento\Framework\App\Cache\TypeListInterface as CacheTypeListInterface;

class MassDelete extends \Magento\Backend\App\Action
{

    /**
     * @var \Magento\Framework\App\Cache\CacheTypeListInterface
     */
    protected $cache;

    /**
     * @var \Magento\Ui\Component\MassAction\Filter
     */
    protected $filter;

    /**
     * @var \Shero\Relabel\Model\ResourceModel\Relabel\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Ui\Component\MassAction\Filter $filter
     * @param \Shero\Relabel\Model\ResourceModel\Relabel\CollectionFactory $collectionFactory
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        CacheTypeListInterface $cache
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->cache = $cache;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $labelsDeleted = 0;
        foreach ($collection->getItems() as $label) {
            $label->delete();
            $labelsDeleted++;
        }
        $this->messageManager->addSuccess(
            __('A total of %1 record(s) have been deleted.', $labelsDeleted)
        );
        $this->cache->invalidate(['full_page','translate']);

        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('*/grid');
    }
}
<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Relabel
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 */
namespace Shero\Relabel\Ui\Component\Form;

use Magento\Framework\View\Element\UiComponent\DataProvider\FilterPool;
use Shero\Relabel\Model\ResourceModel\Relabel\CollectionFactory;

/**
 * Class DataProvider
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var \Shero\Relabel\Model\ResourceModel\Relabel\Collection
     */
    protected $collection;

    /**
     * @var \Magento\Framework\View\Element\UiComponent\DataProvider\FilterPool
     */
    protected $filterPool;

    /**
     * @var array
     */
    protected $loadedData;


    /**
     * DataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param \Shero\Relabel\Model\ResourceModel\Relabel\CollectionFactory $collectionFactory
     * @param \Magento\Framework\View\Element\UiComponent\DataProvider\FilterPool $filterPool
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        FilterPool $filterPool,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
        $this->filterPool = $filterPool;
    }


    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (!$this->loadedData) {
            $items = $this->collection->getItems();
            $result = array();
            foreach ($items as $item) {
                $result['shero_labels'] = $item->getData();
                $this->loadedData[$item->getLabelId()] = $result;
                break;
            }
        }
        return $this->loadedData;
    }
}

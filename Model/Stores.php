<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Relabel
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 */
namespace Shero\Relabel\Model;

use Magento\Store\Model\StoreRepository;

class Stores extends \Magento\Framework\DataObject
    implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var \Magento\Store\Model\StoreRepository
     */
    protected $_storeRepository;

    /**
     * @param \Magento\Store\Model\StoreRepository $storeRepository
     */
    public function __construct(
        StoreRepository $storeRepository
    ) {
        $this->_storeRepository = $storeRepository;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $stores = $this->_storeRepository->getList();
        $websiteIds = array();
        $storeList = array();
        foreach ($stores as $store) {
            $websiteId = $store["website_id"];
            $storeId = $store["store_id"];
            $storeName = $store["name"];
            $storeList[$storeId] = $storeName;
            array_push($websiteIds, $websiteId);
        }
        return $storeList;
    }
}
<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Relabel
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 */
namespace Shero\Relabel\Model\ResourceModel;

use Magento\Framework\App\Config\ScopeConfigInterface;

class Relabel extends \Magento\Translation\Model\ResourceModel\Translate implements
    \Magento\Framework\Translate\ResourceInterface
{

    /**
     * Framework ScopeConfigInterface
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected  $scopeConfig;

    /**
     * Relabel constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Magento\Framework\App\ScopeResolverInterface $scopeResolver
     * @param null $connectionName
     * @param null $scope
     */
    public function __construct
    (
        ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magento\Framework\App\ScopeResolverInterface $scopeResolver,
        $connectionName = null,
        $scope = null
    )
    {
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context, $scopeResolver, $connectionName, $scope);
    }

    /**
     * Define main table
     *
     * @return void
     */
    protected function _construct()
    {
        if($this->_isEnabled()){
            $this->_init('shero_relabel_translation', 'key_id');
        }else{
            parent::_construct();
        }
    }

    /**
     * @return bool
     */
    private function _isEnabled(){
        $enable = $this->scopeConfig->getvalue('relabels/general/enable',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $enable == 1 ? true : false;
    }

}
<?php
/**
 * Copyright © 2017 Shero Designs. All rights reserved.
 * @category Shero Extensions
 * @package Shero_Relabel
 * @author Shero Designs <www.sherodesigns.com>
 * @link https://www.sherodesigns.com/
 */
namespace Shero\Relabel\Model;

use Shero\Relabel\Api\Data\RelabelInterface;

class Relabel extends \Magento\Framework\Model\AbstractExtensibleModel implements RelabelInterface
{
    protected function _construct()
    {
        $this->_init('Shero\Relabel\Model\ResourceModel\Relabel');
    }

    /**
     * @return mixed
     */
    public function getLabelId(){
        return $this->getData(self::LABEL_ID);
    }

    /**
     * @param $entityId
     */
    public function setLabelId($entityId){
        $this->setData(self::LABEL_ID, $entityId);
    }

    /**
     * @return mixed
     */
    public function getLabelString(){
        return $this->getData(self::LABEL_STRING);
    }

    /**
     * @param $string
     */
    public function setLabelString($string){
        $this->setData(self::LABEL_STRING, $string);
    }

    /**
     * @return mixed
     */
    public function getLabelStoreId(){
        return $this->getData(self::LABEL_STORE_ID);
    }

    /**
     * @param $storeId
     */
    public function setLabelStoreId($storeId){
        $this->setData(self::LABEL_STORE_ID, $storeId);
    }

    /**
     * @return mixed
     */
    public function getLabelTranslation(){
        return $this->getData(self::LABEL_TRANS);
    }

    /**
     * @param $translation
     */
    public function setLabelTranslation($translation){
        $this->setData(self::LABEL_TRANS, $translation);
    }

    /**
     * @return mixed
     */
    public function getLabelLocale(){
        return $this->getData(self::LABEL_LOCALE);
    }

    /**
     * @param $locale
     */
    public function setLabelLocale($locale){
        $this->setData(self::LABEL_LOCALE, $locale);
    }

    /**
     * @return mixed
     */
    public function getLabelCrc(){
        return $this->getData(self::LABEL_CRC_STRING);
    }

    /**
     * @param $string
     */
    public function setLabelCrc($string){
        $this->setData(self::LABEL_CRC_STRING, crc32($string));
    }

    /**
     * @param $string,$storeId
     * @return bool | integer
     */
    public function getKeyIdByString($string,$storeId){

        $key_id = $this->getCollection();
        $key_id->addFieldToSelect('key_id');
        $key_id->addFieldToFilter('string',$string);
        $key_id->addFieldToFilter('store_id',$storeId);
        $key_id->setPageSize(1);
        $id = $key_id->getData('key_id');
        if(isset($id[0]['key_id'])){
            return $id[0]['key_id'];
        }else{
            return false;
        }

    }

    /**
     * @return array
     */
    public function getCustomAttributesCodes() {
        return array('key_id', 'string' ,'translate','store_id');
    }
}